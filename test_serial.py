from rest_framework.test import APITestCase
from django.urls import reverse
from rest_framework import status
from django.contrib.auth import get_user_model
from deposits.models import Deposit
from users.models import Phone
from withdrawals.models import Withdrawal
from django.db.models import Sum

class SerialIntegration(APITestCase):

    def setUp(self):

        #create 2 sellers
        self.Seller = get_user_model()

        seller1 = self.Seller.objects.create_user(username='seller1',
                                                password='seller1')
        self.seller1 = seller1.username

        seller2 = self.Seller.objects.create_user(username='seller2',
                                                password='seller2')
        self.seller2 = seller2.username
        
        #create a phone
        self.phone = Phone(phone_number='09390231110')
        self.phone.save()

        #Get access and refresh token with correct username and password for seller1
        auth_url = reverse('token_obtain_pair')
        resp = self.client.post(auth_url, {'username':'seller1',
                                            'password':'seller1'},
                                              format='json')
        
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertTrue('access' in resp.data)
        self.assertTrue('refresh' in resp.data)
        self.token1 = resp.data['access']

        #Get access and refresh token with correct username and password for seller2
        auth_url = reverse('token_obtain_pair')
        resp = self.client.post(auth_url, {'username':'seller2',
                                            'password':'seller2'},
                                              format='json')
        
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertTrue('access' in resp.data)
        self.assertTrue('refresh' in resp.data)
        self.token2 = resp.data['access']
        
        #Get access and refresh token with wrong username and password
        auth_url = reverse('token_obtain_pair')
        resp = self.client.post(auth_url, {'username':'wrong_user',
                                            'password':'wrong_password'},
                                            format='json')
        
        self.assertEqual(resp.status_code, status.HTTP_401_UNAUTHORIZED)



    def do_deposit(self, amount):

        resp = self.client.post(reverse('increase_credit'), {'amount': amount})

        seller1 = self.Seller.objects.get(username=self.seller1)
        seller2 = self.Seller.objects.get(username=self.seller2)

        return resp, seller1, seller2
    



    def do_withdraw(self, amount, token):

        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)
        resp = self.client.post(reverse('charge_phone_number'), {
            'amount': amount,
            'phone_number': self.phone.phone_number})
        return resp
    


    def calculation(self, username):
        """deposit = withdraw + credit"""

        total_deposit_amount = Deposit.objects.filter(seller=username) \
        .aggregate(sum=Sum('amount'))

        total_withrawals_amount = Withdrawal.objects.filter(seller=username) \
        .aggregate(sum=Sum('amount'))

        seller = self.Seller.objects.get(username=username)
        seller_credit = seller.credit

        self.assertEqual(total_deposit_amount['sum'],
                         total_withrawals_amount['sum'] + seller_credit)



    def test_serial(self):
        """Deposit related tests"""

        #when seller is not authenticated
        resp, seller1, seller2 = self.do_deposit(10000)
        self.assertEqual(resp.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(seller1.credit, 0)
        self.assertEqual(seller2.credit, 0)
        self.assertEqual(Deposit.objects.count(), 0)
        
        #when seller1 is authenticated but amount is negative
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token1)
        resp, seller1, seller2 = self.do_deposit(-10000)

        self.assertEqual(seller1.credit, 0)
        self.assertEqual(seller2.credit, 0)
        self.assertEqual(resp.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(Deposit.objects.count(), 0)

        #when seller1 is authenticated and amount is correct
        for i in range(10):
            self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token1)
            resp, seller1, seller2 = self.do_deposit(10000)

            self.assertEqual(seller1.credit, (i+1)*10000)
            self.assertEqual(seller2.credit, 0)
            self.assertEqual(resp.status_code, status.HTTP_200_OK)
            self.assertEqual(Deposit.objects.count(), (i+1))

        #when seller2 is authenticated and amount is correct
        for i in range(10):
            self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token2)
            resp, seller1, seller2 = self.do_deposit(10000)

            self.assertEqual(seller1.credit, 100000)
            self.assertEqual(seller2.credit, (i+1)*10000)
            self.assertEqual(resp.status_code, status.HTTP_200_OK)
            self.assertEqual(Deposit.objects.count(), 10 + (i+1))
        
        """withrawal related tests"""
        for i in range(1000):
            resp = self.do_withdraw(100, self.token1)
            self.assertEqual(resp.status_code, status.HTTP_200_OK)
            
            resp = self.do_withdraw(100, self.token2)
            self.assertEqual(resp.status_code, status.HTTP_200_OK)
            
        phone = Phone.objects.get(phone_number=self.phone.phone_number)
        seller1 = self.Seller.objects.get(username=self.seller1)
        seller2 = self.Seller.objects.get(username=self.seller2)

        self.assertEqual(phone.credit, 200000)
        self.assertEqual(seller1.credit, 0)
        self.assertEqual(seller2.credit, 0)
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertEqual(Deposit.objects.count(), 20)
        self.assertEqual(Withdrawal.objects.count(), 2000)

        #check the Calculations for seller
        self.calculation(seller1)
        self.calculation(seller2)
