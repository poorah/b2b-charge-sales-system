# IMDB crawler  

## Initializing the project  
1. Install ```virtualenv``` on your system:  
	- ```pip install virtualenv```  

2. Activate it in project directory:
    - ```virtualenv venv```  
	- ```source venv/bin/activate```  
	- ```pip install -r requirements.txt```  
   
3. You need ```PostgreSQL``` as DataBase. Set your PostgreSQL configs in ````charge_sales_system/settings.py```` in ```DATABASES``` section

5. run migrations:  
   - ``python3 manage.py makemigrations``  
   - ``python3 manage.py migrate`` 

6. create a superuser for accessing the admin panel:  
   - ``python3 manage.py createsuperuser``  
   - ``enter your email, your phone number and your password``  
7. now you can run the application:  
   - ``python3 manage.py runserver``  
    
## Deploy On Nginx with Gunicorn for better Performance
  
- Gunicorn:
    run these commands:
    ```
    $ cd ~/charging_sales_system

    $ sudo mkdir -pv /var/{log,run}/gunicorn/

    $ sudo chown -cR YOUR_OS_USER:YOUR_OS_USER /var/{log,run}/gunicorn/

    $ gunicorn -c config/gunicorn/dev.py
    ```
    after running these commands application will be available on: 
    http://0.0.0.0:8000/

- NGINX:
    use this config:
```
server {
    server_name               localhost;
    listen                    80;
    location / {
        proxy_pass              http://localhost:8000;
        proxy_set_header        Host $host;
    }

        location /static/ {
        autoindex on;
        root /home/YOUR_OS_USER/PATH/charging_sales_system;
    }
}
````
Then:
```
$ cd /etc/nginx/sites-enabled

$ sudo ln -s ../sites-available/charging_sales_system .

$ sudo systemctl restart nginx
```
NOW you can acess application on http://localhost



## APIs Documentations
you use these postman exports and examples: 
[psotman exports](postman_exports/)


## Tests
       python3 manage.py test

- Unit tests: 
    - [Deposit related](deposits/tests.py)
    - [Withdraw related](withdrawals/tests.py)
    - [Integration Serial Test](test_serial.py)
    - [Concurrency Test](test_real_concurrency.py)


- Load Test:
    I used Locust for load testing.

    - run  ````$ locust````
    - you can do the test on : http://0.0.0.0:8089

    ````
    my load test response time sample for charging api:
        - 1 request per second:
            - Min: 24 ms
            - Average: 47 ms
            - Max: 77 ms
            - 90%ile: 54 ms

        - 20 requests per second:
            - Min: 20 ms
            - Average: 52 ms
            - Max: 720 ms
            - 90%ile: 78 ms
    ````
    


