import requests
from multiprocessing import Process
import os


class MultiProcessTest:

    def get_auth_token(self, username, password):
        resp = requests.post('http://localhost/api/token/', json={
            'username': username,
            'password': password
        })

        if resp.status_code == 200:
            return resp.json()['access']
        return None

    def make_deposit_request(self, auth_token, amount):

        headers = {"Authorization": 'Bearer ' + auth_token}
        resp = requests.post('http://localhost/increase-credit/', json={
        'amount': amount},
        headers=headers)

        print(resp.status_code, resp.json())


    def make_withdraw_request(self, auth_token, amount, phone_number):
        headers = {"Authorization": 'Bearer ' + auth_token}
        resp = requests.post('http://localhost/charge/', json={
        'amount': amount,
        'phone_number': phone_number},
        headers=headers)

        print(resp.status_code, resp.json())


    def start_process(self, username, password, deposit_amount, withdraw_amount, phone_number):
        print(os.getpid())
        auth_token = self.get_auth_token(username, password)
        if auth_token != None:
            self.make_deposit_request(auth_token, deposit_amount)
            self.make_withdraw_request(auth_token, withdraw_amount, phone_number)
        else:
            print('could not obtain auth_token')
def test1():
    test = MultiProcessTest()
    p1 = Process(target=test.start_process, args=('admin', 'admin', 10000, 5000, '09390231110'))
    p2 = Process(target=test.start_process, args=('admin', 'admin', 10000, 10000, '09390231110'))

    p3 = Process(target=test.start_process, args=('seller1', 'seller1', 10000, 5000, '09121111111'))
    p4 = Process(target=test.start_process, args=('seller1', 'seller1', 10000, 10000, '09121111111'))

    p5 = Process(target=test.start_process, args=('seller2', 'seller2', 10000, 5000, '09121111112'))
    p6 = Process(target=test.start_process, args=('seller2', 'seller2', 10000, 10000, '09121111112'))

    p7 = Process(target=test.start_process, args=('seller3', 'seller3', 10000, 5000, '09121111113'))
    p8 = Process(target=test.start_process, args=('seller3', 'seller3', 10000, 10000, '09121111113'))

    p1.start()
    p2.start()
    p3.start()
    p4.start()
    p5.start()
    p6.start()
    p7.start()
    p8.start()


    p1.join()
    p2.join()
    p3.join()
    p4.join()
    p5.join()
    p6.join()
    p7.join()
    p8.join()

def test2():
    test = MultiProcessTest()
    p1 = Process(target=test.start_process, args=('admin', 'admin', 5000, 40000, '09390231110'))
    p2 = Process(target=test.start_process, args=('admin', 'admin', 5000, 40000, '09390231110'))

    p3 = Process(target=test.start_process, args=('admin', 'admin', 5000, 40000, '09390231110'))
    p4 = Process(target=test.start_process, args=('admin', 'admin', 5000, 40000, '09390231110'))

    p5 = Process(target=test.start_process, args=('admin', 'admin', 5000, 40000, '09390231110'))
    p6 = Process(target=test.start_process, args=('admin', 'admin', 5000, 40000, '09390231110'))

    p7 = Process(target=test.start_process, args=('admin', 'admin', 5000, 40000, '09390231110'))
    p8 = Process(target=test.start_process, args=('admin', 'admin', 5000, 40000, '09390231110'))

    p1.start()
    p2.start()
    p3.start()
    p4.start()
    p5.start()
    p6.start()
    p7.start()
    p8.start()


    p1.join()
    p2.join()
    p3.join()
    p4.join()
    p5.join()
    p6.join()
    p7.join()
    p8.join()

test1()
#test1()