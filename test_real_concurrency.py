from rest_framework.test import APITransactionTestCase
from django.urls import reverse
from rest_framework import status
from django.contrib.auth import get_user_model
from deposits.models import Deposit
from users.models import Phone
from withdrawals.models import Withdrawal
from django.db.models import Sum
from threading import Thread
from django.db import connections

class Concurrency(APITransactionTestCase):
    """in this senario a seller make a request to increase the charge
        then will request to charge a phone number
        this senario will run cocurrently by 4 thread """

    def setUp(self):

        self.Seller = get_user_model()
        seller = self.Seller.objects.create_user(username='seller',
                                                password='seller')
        seller.save()
                
        self.phone = Phone(phone_number='09390231110')
        self.phone.save()


    def do_deposit(self, amount):
        """request to make a deposit"""

        resp = self.client.post(reverse('increase_credit'), {'amount': amount})
        return resp
    

    def do_withdraw(self, amount):
        """request to make a withdraw"""

        resp = self.client.post(reverse('charge_phone_number'), {
            'amount': amount,
            'phone_number': self.phone.phone_number})
        return resp
    

    def calculation(self, user):
        """check this: deposit = withdraw + credit"""

        total_deposit_amount = Deposit.objects.filter(seller=user) \
        .aggregate(sum=Sum('amount'))

        total_withrawals_amount = Withdrawal.objects.filter(seller=user) \
        .aggregate(sum=Sum('amount'))

        seller_credit = user.credit

        self.assertEqual(total_deposit_amount['sum'],
                         total_withrawals_amount['sum'] + seller_credit)


    def do_proccess(self, user, withdraw_amount, deposit_amount):
        """first make a deposit then do the withdraw"""

        # Authenticate
        auth_url = reverse('token_obtain_pair')
        resp = self.client.post(auth_url, {'username':user,
                                            'password':user},
                                              format='json')
        
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertTrue('access' in resp.data)
        self.assertTrue('refresh' in resp.data)
        self.token = resp.data['access']
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)


        """Deposit related"""

        #when seller is authenticated and amount is correct
        resp = self.do_deposit(deposit_amount)
        print(resp.data)

        """withrawal related tests"""
        resp = self.do_withdraw(withdraw_amount)
        print(resp.data)

        #We don't need to do this in our views, because django will automatically
        #close the connection after processing the request
        connections.close_all()

    def run_multithread(self, withdraw_amount, deposit_amount):

        t1 = Thread(target=self.do_proccess, args=('seller',withdraw_amount, deposit_amount))
        t2 = Thread(target=self.do_proccess, args=('seller',withdraw_amount, deposit_amount))
        t3 = Thread(target=self.do_proccess, args=('seller',withdraw_amount, deposit_amount))
        t4 = Thread(target=self.do_proccess, args=('seller',withdraw_amount, deposit_amount))

        # start the threads
        t1.start()
        t2.start()
        t3.start()
        t4.start()


        # wait for the threads to complete
        t1.join()
        t2.join()
        t3.join()
        t4.join()

    def test_one(self):

        self.run_multithread(5000, 10000)

        phone = Phone.objects.get(phone_number=self.phone)
        self.assertEqual(phone.credit, 20000)

        seller = self.Seller.objects.get(username='seller')
        self.assertEqual(seller.credit, 20000)

        self.calculation(seller)

        self.assertEqual(Deposit.objects.count(), 4)
        self.assertEqual(Withdrawal.objects.count(), 4)

    def test_two(self):
            """
            first time -> fail (seller's credit  = 1000, withdraw = 4000)
            second time -> fail (seller's credit = 2000, withdraw = 4000)
            third time -> fail (seller's credit  = 3000, withdraw = 4000)
            fourth time -> success (seller's credit = 4000, withdraw = 4000)
            """

            self.run_multithread(4000, 1000)

            phone = Phone.objects.get(phone_number=self.phone)
            self.assertEqual(phone.credit, 4000)

            seller = self.Seller.objects.get(username='seller')
            self.assertEqual(seller.credit, 0)

            self.assertEqual(Deposit.objects.count(), 4)
            self.assertEqual(Withdrawal.objects.count(), 1)

            self.calculation(seller)
