from rest_framework.test import APITestCase
from django.urls import reverse
from rest_framework import status
from django.contrib.auth import get_user_model
from .models import Withdrawal
from users.models import Phone

Seller = get_user_model()

class WithdrawTest(APITestCase):

    def setUp(self):
        """setUp function before running the tests"""

        #create a seller
        Seller = get_user_model()
        self.seller = Seller.objects.create_user(username='seller1',
                                                password='seller1',
                                                credit=10000)
        self.seller.save()

        # create a phone number       
        self.phone = Phone(phone_number='09390231110')
        self.phone.save()

        #Get access and refresh token with correct username and password
        auth_url = reverse('token_obtain_pair')
        resp = self.client.post(auth_url, {'username':'seller1',
                                            'password':'seller1'},
                                              format='json')
        #Test
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertTrue('access' in resp.data)
        self.assertTrue('refresh' in resp.data)
        self.access_token = resp.data['access']
        
        #Get access and refresh token with wrong username and password
        auth_url = reverse('token_obtain_pair')
        resp = self.client.post(auth_url, {'username':'wrong_user',
                                            'password':'wrong_password'},
                                            format='json')
        #Test
        self.assertEqual(resp.status_code, status.HTTP_401_UNAUTHORIZED)


    def test_check_authentication(self):

        #when seller is not authenticated
        resp = self.client.post(reverse('charge_phone_number'),
                                 {'amount': 10000,
                                  'phone_number':self.phone.phone_number})

        self.assertEqual(resp.status_code, status.HTTP_401_UNAUTHORIZED)

        seller = Seller.objects.get(username=self.seller.username)
        self.assertEqual(seller.credit, 10000)

        phone = Phone.objects.get(phone_number=self.phone.phone_number)
        self.assertEqual(phone.credit, 0)

        self.assertEqual(Withdrawal.objects.count(), 0)
        

        #when seller is authenticated
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.access_token)

        resp = self.client.post(reverse('charge_phone_number'),
                                 {'amount': 10000,
                                  'phone_number':self.phone.phone_number})
        
        self.assertEqual(resp.status_code, status.HTTP_200_OK)

        seller = Seller.objects.get(username=self.seller.username)
        self.assertEqual(seller.credit, 0)

        phone = Phone.objects.get(phone_number=self.phone.phone_number)
        self.assertEqual(phone.credit, 10000)

        self.assertEqual(Withdrawal.objects.count(), 1)


    def test_check_amount(self):
    
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.access_token)


        #when amount is not in request
        resp = self.client.post(reverse('charge_phone_number'),
                            {'phone_number':self.phone.phone_number})

        self.assertEqual(resp.status_code, status.HTTP_403_FORBIDDEN)

        seller = Seller.objects.get(username=self.seller.username)
        self.assertEqual(seller.credit, 10000)

        phone = Phone.objects.get(phone_number=self.phone.phone_number)
        self.assertEqual(phone.credit, 0)

        self.assertEqual(Withdrawal.objects.count(), 0)


        #when amount is negative
        resp = self.client.post(reverse('charge_phone_number'),
                                 {'amount': -10000,
                                  'phone_number':self.phone.phone_number})
        
        self.assertEqual(resp.status_code, status.HTTP_403_FORBIDDEN)

        seller = Seller.objects.get(username=self.seller.username)
        self.assertEqual(seller.credit, 10000)

        phone = Phone.objects.get(phone_number=self.phone.phone_number)
        self.assertEqual(phone.credit, 0)

        self.assertEqual(Withdrawal.objects.count(), 0)


        #when amount is ok
        resp = self.client.post(reverse('charge_phone_number'),
                                 {'amount': 10000,
                                  'phone_number':self.phone.phone_number})
        
        self.assertEqual(resp.status_code, status.HTTP_200_OK)

        seller = Seller.objects.get(username=self.seller.username)
        self.assertEqual(seller.credit, 0)

        phone = Phone.objects.get(phone_number=self.phone.phone_number)
        self.assertEqual(phone.credit, 10000)

        self.assertEqual(Withdrawal.objects.count(), 1)


    def test_check_phone_number(self):
    
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.access_token)


        #when phone number is not in request
        resp = self.client.post(reverse('charge_phone_number'),
                            {'amount':10000})

        self.assertEqual(resp.status_code, status.HTTP_403_FORBIDDEN)

        seller = Seller.objects.get(username=self.seller.username)
        self.assertEqual(seller.credit, 10000)

        phone = Phone.objects.get(phone_number=self.phone.phone_number)
        self.assertEqual(phone.credit, 0)

        self.assertEqual(Withdrawal.objects.count(), 0)

        #when phone number  is ok
        resp = self.client.post(reverse('charge_phone_number'),
                                 {'amount': 10000,
                                  'phone_number':self.phone.phone_number})
        
        self.assertEqual(resp.status_code, status.HTTP_200_OK)

        seller = Seller.objects.get(username=self.seller.username)
        self.assertEqual(seller.credit, 0)

        phone = Phone.objects.get(phone_number=self.phone.phone_number)
        self.assertEqual(phone.credit, 10000)

        self.assertEqual(Withdrawal.objects.count(), 1)



    def test_check_user_credit(self):
    
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.access_token)

        #when user's credit is less than amount
        resp = self.client.post(reverse('charge_phone_number'),
                                 {'amount': 20000,
                                  'phone_number':self.phone.phone_number})
        
        self.assertEqual(resp.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR)

        seller = Seller.objects.get(username=self.seller.username)
        self.assertEqual(seller.credit, 10000)

        phone = Phone.objects.get(phone_number=self.phone.phone_number)
        self.assertEqual(phone.credit, 0)

        self.assertEqual(Withdrawal.objects.count(), 0)