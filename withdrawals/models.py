from django.db import models, transaction
from users.models import Phone
from django.contrib.auth import get_user_model
Seller = get_user_model()

class Withdrawal(models.Model):
    
    seller = models.ForeignKey(Seller,
                               on_delete=models.PROTECT,
                               related_name='withdrawals')
    
    phone_number = models.ForeignKey(Phone,
                                     on_delete=models.PROTECT,
                                     related_name='withdrawals')
    
    amount = models.PositiveIntegerField(
        null=False,
        blank=False)

    date_and_time = models.DateTimeField(auto_now_add=True)

    @transaction.atomic
    def make_withdraw(self, user, phone, amount):

        phone = Phone.objects.get(phone_number=phone)

        user.decrease_credit(amount)
        phone.increase_credit(amount)
        
        self.seller=user
        self.amount=amount
        self.phone_number=phone
        self.save()

    def __str__(self):
        return 'Withdraw-' + str(self.id)
