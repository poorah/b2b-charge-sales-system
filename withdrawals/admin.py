from django.contrib import admin
from .models import Withdrawal

class WithdrawalAdmin(admin.ModelAdmin):
    list_display = ['id', 'seller', 'phone_number', 'amount', 'date_and_time']
    readonly_fields = ('date_and_time',)

admin.site.register(Withdrawal, WithdrawalAdmin)
