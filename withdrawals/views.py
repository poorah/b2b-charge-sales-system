from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from django.db.models import F
from rest_framework import serializers
from .models import Withdrawal
from django.db import transaction
from rest_framework import status
import re
from django.core.exceptions import ValidationError
from users.models import Phone

class WithdrawSerializer(serializers.Serializer):
    """amonut and phone_number validations in request"""

    amount = serializers.IntegerField(required=True, min_value=0)
    phone_number = serializers.CharField(required=True)

    def validate_phone_number(self, attrs):
        if bool(re.search(r'^((0)9\d{9})$', attrs)):
            try:
                phone = Phone.objects.get(phone_number=attrs)
                return phone
            except:
               raise ValidationError('phone number is not found in databse!') 
        raise ValidationError('phone number is not valid!')



@api_view(['POST'])
@permission_classes([IsAuthenticated])
def charge_phone_number(request):

    serializer = WithdrawSerializer(data=request.data)

    if serializer.is_valid():

        user = request.user
        amount = request.data['amount']
        phone_number = request.data['phone_number']
        try:
            withdraw = Withdrawal()
            withdraw.make_withdraw(user,phone_number,amount)
            
            return Response({"msg": f"{user} charged {phone_number} +{amount}!"})
            
        except Exception as e:
            print(str(e))
            return Response({'erorr': 'can not do the operation'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        
        
    return Response(serializer.errors, status=status.HTTP_403_FORBIDDEN)
