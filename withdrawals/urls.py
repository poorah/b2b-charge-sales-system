from django.urls import path
from .views import charge_phone_number

urlpatterns = [

    path('', charge_phone_number, name='charge_phone_number')
]