from locust import HttpUser, task, between



class MarketAPI(HttpUser):
    wait_time = between(0, 1)

    def on_start(self):
        self.login()

    @task(1)
    def login(self):
        response = self.client.post(f"/api/token/", json={"username": 'admin', "password": 'admin'})
        if response.json().get('errors'):
            print(response.text)
        self.access_token = response.json()['access']
        self.refresh_token = response.json()['refresh']

    @task(2)
    def charge(self):
        self.client.post(f"/charge/", headers={'Authorization': f'Bearer {self.access_token}'},
                         data={"amount": "1",
                               "phone_number": "09390231110"})