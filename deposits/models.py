from django.db import models, transaction
from django.contrib.auth import get_user_model
Seller = get_user_model()


class Deposit(models.Model):
    """Deposits logs model"""
    
    seller = models.ForeignKey(Seller,
                               on_delete=models.PROTECT,
                               related_name='deposits')
    
    amount = models.PositiveIntegerField(
        null=False,
        blank=False)

    date_and_time = models.DateTimeField(auto_now_add=True)

    @transaction.atomic
    def make_deposit(self, user, amount):

        user.increase_credit(amount)
        
        self.seller = user
        self.amount = amount
        self.save()

    def __str__(self):
        return 'Deposit-' + str(self.id)

    