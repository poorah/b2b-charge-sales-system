from django.urls import path
from .views import increase_credit

urlpatterns = [

    path('', increase_credit, name='increase_credit')
]