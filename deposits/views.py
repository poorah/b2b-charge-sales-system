from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from django.db.models import F
from rest_framework import serializers
from .models import Deposit
from django.db import transaction
from rest_framework import status


class AmountSeralizer(serializers.Serializer):
    """validate amount in request"""

    amount = serializers.IntegerField(required=True, min_value=0)



@api_view(['POST'])
@permission_classes([IsAuthenticated])
def increase_credit(request):
    """after calling this api seller's credit will increase"""


    serializer = AmountSeralizer(data=request.data)

    if serializer.is_valid():

        user = request.user
        amount = serializer.data['amount']
        try: 
            deposit= Deposit()
            deposit.make_deposit(user, amount)

            return Response({"msg": f"{user}'s credit, increased +{amount}!"})
            
        except:
            return Response({'erorr': 'could not do the operation'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    
    return Response(serializer.errors, status=status.HTTP_403_FORBIDDEN)
