from rest_framework.test import APITestCase
from django.urls import reverse
from rest_framework import status
from django.contrib.auth import get_user_model
from .models import Deposit
Seller = get_user_model()

class DepositTest(APITestCase):

    def setUp(self):
        """setUp function before running the tests"""

        #create a seller
        Seller = get_user_model()
        self.seller = Seller.objects.create_user(username='seller1',
                                                password='seller1')
        self.seller.save()

        #Get access and refresh token with correct username and password
        auth_url = reverse('token_obtain_pair')
        resp = self.client.post(auth_url, {'username':'seller1',
                                            'password':'seller1'},
                                              format='json')
        #Test
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertTrue('access' in resp.data)
        self.assertTrue('refresh' in resp.data)
        self.access_token = resp.data['access']
        
        #Get access and refresh token with wrong username and password
        auth_url = reverse('token_obtain_pair')
        resp = self.client.post(auth_url, {'username':'wrong_user',
                                            'password':'wrong_password'},
                                            format='json')
        #Test
        self.assertEqual(resp.status_code, status.HTTP_401_UNAUTHORIZED)


    def test_check_authentication(self):

        #when seller is not authenticated
        resp = self.client.post(reverse('increase_credit'), {'amount': 1000})

        self.assertEqual(resp.status_code, status.HTTP_401_UNAUTHORIZED)

        seller = Seller.objects.get(username=self.seller.username)
        self.assertEqual(seller.credit, 0)

        self.assertEqual(Deposit.objects.count(), 0)
        

        #when seller is authenticated
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.access_token)

        resp = self.client.post(reverse('increase_credit'), {
            'amount': 1000
            }, format='json')
        
        self.assertEqual(resp.status_code, status.HTTP_200_OK)

        seller = Seller.objects.get(username=self.seller.username)
        self.assertEqual(seller.credit, 1000)

        self.assertEqual(Deposit.objects.count(), 1)


    def test_check_amount(self):
    
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.access_token)


        #when amount is not in request
        resp = self.client.post(reverse('increase_credit'), {"wrong": 1000})

        self.assertEqual(resp.status_code, status.HTTP_403_FORBIDDEN)

        seller = Seller.objects.get(username=self.seller.username)
        self.assertEqual(seller.credit, 0)

        self.assertEqual(Deposit.objects.count(), 0)
        

        #when amount is negative
        resp = self.client.post(reverse('increase_credit'), {
            'amount': -1000
            }, format='json')
        
        self.assertEqual(resp.status_code, status.HTTP_403_FORBIDDEN)

        seller = Seller.objects.get(username=self.seller.username)
        self.assertEqual(seller.credit, 0)

        self.assertEqual(Deposit.objects.count(), 0)


        #when amount is ok
        resp = self.client.post(reverse('increase_credit'), {
            'amount': 1000
            }, format='json')
        
        self.assertEqual(resp.status_code, status.HTTP_200_OK)

        seller = Seller.objects.get(username=self.seller.username)
        self.assertEqual(seller.credit, 1000)

        self.assertEqual(Deposit.objects.count(), 1)