from django.contrib import admin
from .models import Deposit


class DepositAdmin(admin.ModelAdmin):
    list_display = ['id', 'seller', 'amount', 'date_and_time']
    readonly_fields = ['date_and_time']

admin.site.register(Deposit, DepositAdmin)