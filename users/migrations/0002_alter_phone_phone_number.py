# Generated by Django 4.1.7 on 2023-04-01 12:23

from django.db import migrations, models
import users.models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='phone',
            name='phone_number',
            field=models.CharField(max_length=13, unique=True, validators=[users.models.validate_phone_number]),
        ),
    ]
