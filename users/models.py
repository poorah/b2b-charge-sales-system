from django.db import models
from django.contrib.auth.models import AbstractUser
from django.core.exceptions import ValidationError
import re
from django.db.models import F

def validate_phone_number(phone_number):
    """iranian format phone number validator"""

    if bool(re.search(r'^((0)9\d{9})$', phone_number)):
        return phone_number
    raise ValidationError('phone number is not valid!')
    

class Seller(AbstractUser):
    """custom user(seller) model"""

    credit = models.PositiveIntegerField(default=0) 

    def increase_credit(self, amount):
        self.credit = F('credit') + amount
        self.save()   

    def decrease_credit(self, amount):
        self.credit = F('credit') - amount
        self.save()
    class Meta:
        verbose_name = 'seller'
        verbose_name_plural = 'sellers'


class Phone(models.Model):

    phone_number = models.CharField(
        max_length=11,
        unique=True,
        null=False,
        blank=False,
        validators=[validate_phone_number])
    
    credit = models.PositiveIntegerField(default=0)

    def increase_credit(self, amount):
        self.credit = F('credit') + amount
        self.save() 
    
    def __str__(self):
        return self.phone_number


