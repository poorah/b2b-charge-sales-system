from django.contrib import admin
from django.contrib.auth import get_user_model
from .models import Phone
from deposits.models import Deposit
from withdrawals.models import Withdrawal
Seller = get_user_model()



class DepositsInline(admin.TabularInline):
    """Inline class for showing each user's deposits in admin panel"""

    model = Deposit
    readonly_fields = ['date_and_time']
    extra = 0

class WithdrawalsInline(admin.TabularInline):
    """Inline class for showing each user's withdrawals in admin panel"""

    model = Withdrawal
    readonly_fields = ['date_and_time']
    extra = 0


class SellerAdmin(admin.ModelAdmin):
    list_display = ['username', 'first_name', 'last_name', 'credit', 'is_staff']
    inlines = [DepositsInline, WithdrawalsInline]

admin.site.register(Seller, SellerAdmin)


class PhoneAdmin(admin.ModelAdmin):
    list_display = ['phone_number', 'credit']
    inlines = [WithdrawalsInline]

admin.site.register(Phone, PhoneAdmin)
